Orior is an application for EU Covid-19 monitoring.

This application was developed on the platform : Android studio using Kotlin.

-- Orior --


Information has the power to alter people’s perception about the world, for better or for worse.
A popular but false coronavirus conspiracy theory is that the virus was engineered in a lab and released into the world deliberately. This idea has already been debunked, but lots of people still believe this is the truth.
Coronavirus has also shaped our life, physically. Some nations are in a lockdown state. All this measures to contrast the virus are necessary but they carry lots of problems to the society, and with them, a state of uncertainty among the population.
In order to solve the “too much informations” problem, we’ve chosen to create a safe environment with easy to access data only from trusted sources.
Accessing data from government websites is doable, but the point is that data is not adapted to a smartphone experience. That’s why we’ve come up with Orior.
Orior allows to see general European statistics for the coronavirus spread and an easy way to access emergency numbers.
Charts in various forms allows the user to have a clear perception and an immediate feedback on how the Covid-19 contagion is spreading all around Europe.

![](/Images/Screen1.png)
![](/Images/Screen2.png)
![](/Images/Screen3.png)
