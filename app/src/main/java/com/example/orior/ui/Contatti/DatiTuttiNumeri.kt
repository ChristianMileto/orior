package com.example.orior.ui.Contatti

import com.google.gson.annotations.SerializedName

 class EmergencyNumbers (

    @SerializedName("region_name") val region_name : String,
    @SerializedName("nation_name") val nation_name : String,
    @SerializedName("phone_number") val phone_number : Int
)
class DatiTuttiNumeri (

    @SerializedName("EmergencyNumbers") val emergencyNumbers : List<EmergencyNumbers>
)