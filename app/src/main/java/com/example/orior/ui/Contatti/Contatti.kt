package com.example.orior.ui.Contatti

import android.Manifest
import android.Manifest.permission.CALL_PHONE
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager


import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.activity.addCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.Adapter.RvAdapter4
import com.example.orior.DataCard.datiCard4
import com.example.orior.R
import com.example.orior.okHttp.okHttp1


class contatti : Fragment() {

    companion object {
        fun newInstance() = contatti()
    }

    private lateinit var viewModel: ContattiViewModel
    var countries = arrayListOf<String>("Austria","Belgium","Bulgaria","Croatia","Cyprus","Czech Republic","Denmark","Estonia","Finland","France","Germany","Greece","Hungary","Ireland","Italy","Latvia","Luxembourg","Malta","Netherlands","Poland","Portugal","Romania","Slovakia","Slovenia","Spain","Sweden", "United Kingdom")
    lateinit var v : View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            // Handle the back button event
            activity?.finish()
        }
        v = inflater.inflate(R.layout.activity_main2, container, false)
        return v
    }

    @SuppressLint("WrongConstant")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.Recyclevie)
        recyclerView.layoutManager = LinearLayoutManager(view.context, LinearLayout.VERTICAL, false)
        var lista = arrayListOf<datiCard4>()

            countries.forEach {
                var numero = 0
                when(it){
                    "Italy"->{
                        numero = 1500
                    }
                    "Austria"->{
                        numero = 144
                    }
                    else -> numero = 112
                }
                lista.add(
                    datiCard4(
                        it,
                        numero.toString()
                    )
                )
            }


            activity?.runOnUiThread {
                val rvAdapter = RvAdapter4(lista)
                recyclerView.adapter = rvAdapter
                rvAdapter.onItemClick={numero ->
                    call(numero)
                }
            }
        }

    @SuppressLint("MissingPermission")
    fun call (number : String){
        if(setupPermissions()) {
            val uri = "tel:" + number
            val intent = Intent(Intent.ACTION_CALL)
            intent.data = Uri.parse(uri)
            this.startActivity(intent)
        }
    }
    private fun setupPermissions() : Boolean {
        if (context?.let { checkSelfPermission(it,CALL_PHONE) }
            != PackageManager.PERMISSION_GRANTED) {

            activity?.let { ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.CALL_PHONE), 1) }
            return false
        }
        return true
    }

}
