package com.example.orior.ui.home

import RvAdapter
import android.os.Bundle
import android.os.ProxyFileDescriptorCallback
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.DataCard.PassValue
import com.example.orior.DatiJson.DatiNazionali
import com.example.orior.R
import com.example.orior.DataCard.datiCard
import com.example.orior.DatiJson.Csv.DatiArray

import com.example.orior.DatiJson.Csv.DatiUE
import com.example.orior.okHttp.okHttp1
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    val dataList = ArrayList<datiCard>()
    var countriesItalia = arrayListOf <String> ("Andorra", "Albania", "Austria", "Belgio", "Bielorussia", "Bosnia ed Erzegovina", "Bulgaria", "Croazia", "Cipro", "Repubblica Ceca", "Danimarca", "Estonia" , "Isole Faroe", "Finlandia", "Francia", "Germania", "Gibilterra", "Grecia", "Ungheria", "Islanda", "Irlanda", "Italia", "Kosovo", "Lettonia", "Lituania", "Lussemburgo", "Malta", "Moldavia", "Monaco", "Macedonia del Nord", "Paesi Bassi", "Polonia", "Portogallo", "Romania", "San Marino", "Slovacchia", "Slovenia", "Spagna", "Svezia", "Svizzera", "Ucraina", "Regno Unito")
    var countries = arrayListOf<String>("Andorra","Albania","Austria","Belgium","Belarus","Bosnia and Herzegovina","Bulgaria","Croatia","Cyprus","Czech Republic","Denmark","Estonia","Faroe Islands","Finland","France","Germany","Gibraltar","Greece","Hungary","Iceland","Ireland","Italy","Kosovo","Latvia", "Lithuania","Luxembourg","Malta","Moldova","Monaco","North Macedonia","Netherlands","Poland","Portugal","Romania","San Marino","Slovakia","Slovenia","Spain","Sweden", "Switzerland","Ukraine","United Kingdom")
    val europaconfermati = arrayListOf<DatiUE>()
    val europaguaritii = arrayListOf<DatiUE>()
    val eurodeceduti = arrayListOf<DatiUE>()
    val giorni = arrayListOf<String>()
   lateinit var DatiArray : DatiArray

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.activity_main2,container,false)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        val recyclerView = view.findViewById<RecyclerView>(R.id.Recyclevie)

        recyclerView.layoutManager = LinearLayoutManager(view.context, LinearLayout.VERTICAL, false)





        inizializeTableWithContent{

            activity?.runOnUiThread {
                val rvAdapter = RvAdapter(dataList)
                recyclerView.adapter = rvAdapter

            }
        }



        }
    fun inizializeTableWithContent( callback: (Boolean)->Unit){
        okHttp1.shared.getDatiEuropeiDeceduti {m->
            okHttp1.shared.getDatiEuropeiGuariti { s ->
                okHttp1.shared.getDatiEuropeiConfermati {
                    parse(it, 0)
                    parse(s, 1)
                    parse(m, 2)
                    val m = getsommaedifferenza(0)
                    val m1 = getsommaedifferenza(1)
                    val m2 = getsommaedifferenza(2)
                    dataList.add(
                        datiCard(
                            getString(R.string.conf),
                            m[0].toString(),
                            m[1].toString(),
                            null,
                            null,
                            null
                        )
                    )
                    dataList.add(datiCard( getString(R.string.dec), m2[0].toString(), m2[1].toString(), null, null,null))
                    dataList.add(
                        datiCard(
                            getString(R.string.guar),
                            m1[0].toString(),
                            m1[1].toString(),
                            null,
                            null,
                            null

                        )
                    )
                    var italiano = arrayListOf<String>()
                    if(Locale.getDefault().getLanguage() == "it"){
                     italiano =  countriesItalia
                    }
                    println(europaguaritii.count())
                    println(countriesItalia.count())
                    DatiArray = DatiArray(europaconfermati,europaguaritii,eurodeceduti,giorni,italiano,countries)
                    PassValue.shared.setdatiarr(DatiArray)
                    dataList.add(datiCard(null,null,null,null,null,DatiArray))
                    dataList.add(datiCard(null,null,null,null,null,DatiArray))
                    dataList.add(datiCard(null,null,null,null,null,DatiArray))
                    dataList.add(datiCard(null,null,null,null,null,DatiArray))
                    dataList.add(datiCard(null,null,null,null,null,DatiArray))
                    callback(true)

                }
            }
        }
    }
    fun getsommaedifferenza(scelta : Int) : ArrayList<Int>{
        val int = arrayListOf<Int>()
        var somma = 0
        var statistica = 0
        when(scelta) {
          0-> {

              europaconfermati.forEach {
                  somma += it.totale_casi!!.last()
                  statistica += it.totale_casi[it.totale_casi.count() - 2]
              }
              val sot = somma - statistica
              int.add(somma)
              int.add(sot)
          }
            1->{
                europaguaritii.forEach {
                    somma += it.totale_casi!!.last()
                    statistica += it.totale_casi[it.totale_casi.count() - 2]
                }
                val sot = somma - statistica
                int.add(somma)
                int.add(sot)
            }
            2->{
                eurodeceduti.forEach {
                    somma += it.totale_casi!!.last()
                    statistica += it.totale_casi[it.totale_casi.count() - 2]
                }
                val sot = somma - statistica
                int.add(somma)
                int.add(sot)
            }
        }
        return int
    }
    fun parse(it : String?,scelta : Int){
        if (it != null) {
            var flag = false
            var riga = it.split("\n")
            riga.forEach {
                var colonne = it.split(",")

                for (i in 0..countries.count().minus(1)) {
                    if(i == 0 && !flag && scelta == 0){
                        for (k in 4..colonne.count()-1){
                            giorni.add(colonne[k])
                        }
                    }
                    flag = true
                    if (colonne.count() > 1){
                        if (colonne[1] == countries[i]) {

                            var interi = arrayListOf<Int>()
                            for (j in 4..colonne.count() - 1) {
                                interi.add(colonne[j].toFloat().toInt())
                            }
                            when(scelta){
                                0->{
                                    europaconfermati.add(
                                        DatiUE(
                                            colonne[0],
                                            colonne[1],
                                            colonne[2],
                                            colonne[3],
                                            interi
                                        )
                                    )
                                }
                                1-> {
                                    europaguaritii.add(
                                        DatiUE(
                                            colonne[0],
                                            colonne[1],
                                            colonne[2],
                                            colonne[3],
                                            interi
                                        )
                                    )
                                }
                                2-> {
                                    eurodeceduti.add(
                                        DatiUE(
                                            colonne[0],
                                            colonne[1],
                                            colonne[2],
                                            colonne[3],
                                            interi
                                        )
                                    )
                                }
                            }

                        }
                    }
                }

            }

        }
    }
    companion object{
        val shared = HomeFragment()
    }
    }

