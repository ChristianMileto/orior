package com.example.orior.DatiJson

import com.google.gson.annotations.SerializedName

class DatiProvincia
      (
    @SerializedName("data") val data : String,
    @SerializedName("stato") val stato : String,
    @SerializedName("codice_regione") val codice_regione : Int,
    @SerializedName("denominazione_regione") val denominazione_regione : String,
    @SerializedName("codice_provincia") val codice_provincia : Int,
    @SerializedName("denominazione_provincia") val denominazione_provincia : String,
    @SerializedName("sigla_provincia") val sigla_provincia : String,
    @SerializedName("lat") val lat : Double,
    @SerializedName("long") val long : Double,
    @SerializedName("totale_casi") val totale_casi : Int,
    @SerializedName("note_it") val note_it : String,
    @SerializedName("note_en") val note_en : String
    )
