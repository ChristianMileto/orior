package com.example.orior

import RvAdapter3
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.DataCard.PassValue
import com.example.orior.DataCard.datiCard3
import com.example.orior.DatiJson.DatiRegionali
import com.example.orior.okHttp.okHttp1
import com.google.android.material.internal.ContextUtils.getActivity
import java.util.*
import kotlin.collections.ArrayList


class Main4Activity : AppCompatActivity() {
    val dataList = ArrayList<datiCard3>()
    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val recyclerView = findViewById<RecyclerView>(R.id.Recyclevie)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)


        var nomeRegione = ""
        var regione = ""
        if (Locale.getDefault().getLanguage() == "it"){
            nomeRegione = intent.extras?.getString("regione").toString()
            regione = intent.extras?.getString("regioneIta").toString()
            this.setTitle(regione)
        }else{
            nomeRegione = intent.extras?.getString("regione").toString()
            this.setTitle(nomeRegione)
        }




        var dati = PassValue.shared.getDati1()
        for (i in 0..dati.confermati.count()-1){
            var confermati = 0
            var confermatisot = 0
            var guariti = 0
            var guaritisot = 0
            var deceduti = 0
            var decedutisot = 0
            var regione = ""

            if(dati.confermati[i].stato == "" && dati.confermati[i].Region == nomeRegione){
                regione = dati.confermati[i].Region.toString()
                confermati = dati.confermati[i].totale_casi!!.last()
                confermatisot = confermati.minus(dati.confermati[i].totale_casi!![dati.confermati[i].totale_casi!!.count()-2])
                deceduti = dati.deceduti[i].totale_casi!!.last()
                decedutisot  = deceduti.minus(dati.deceduti[i].totale_casi!![dati.deceduti[i].totale_casi!!.count()-2])
                guariti = dati.guariti[i].totale_casi!!.last()
                guaritisot = guariti.minus(dati.guariti[i].totale_casi!![dati.guariti[i].totale_casi!!.count()-2])
                dataList.add(datiCard3(confermati.toString(),confermatisot.toString(),guariti.toString(),guaritisot.toString(),deceduti.toString(),decedutisot.toString(),null,null,null,null))
                dataList.add(datiCard3(confermati.toString(),confermatisot.toString(),guariti.toString(),guaritisot.toString(),deceduti.toString(),decedutisot.toString(),null,nomeRegione,null,null))


                if(nomeRegione == "Italy"){
                  italyStart(recyclerView,nomeRegione)
            }else{
                   // dataList.add(datiCard3(confermati.toString(),confermatisot.toString(),guariti.toString(),guaritisot.toString(),deceduti.toString(),decedutisot.toString(),null,nomeRegione,null,null))
                    dataList.add(datiCard3(confermati.toString(),confermatisot.toString(),guariti.toString(),guaritisot.toString(),deceduti.toString(),decedutisot.toString(),null,nomeRegione,null,null))
                    dataList.add(datiCard3(confermati.toString(),confermatisot.toString(),guariti.toString(),guaritisot.toString(),deceduti.toString(),decedutisot.toString(),null,nomeRegione,null,null))
                    dataList.add(datiCard3(confermati.toString(),confermatisot.toString(),guariti.toString(),guaritisot.toString(),deceduti.toString(),decedutisot.toString(),null,nomeRegione,null,null))
                    dataList.add(datiCard3(confermati.toString(),confermatisot.toString(),guariti.toString(),guaritisot.toString(),deceduti.toString(),decedutisot.toString(),null,nomeRegione,null,null))
                    runOnUiThread {
                        val rvAdapter = RvAdapter3(dataList)
                        recyclerView.adapter = rvAdapter

                    }
                }
            }

        }


        }





fun italyStart(recyclerView : RecyclerView,nomeRegione : String){
    okHttp1.shared.getDatiNazionali {s->
        okHttp1.shared.getDatiRegionaliLatest {

            dataList.add(
                datiCard3(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    it,
                    nomeRegione,
                    null
                    , null
                )
            )
            dataList.add(
                datiCard3(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    nomeRegione,
                    null
                    , s
                )
            )
            dataList.add(
                datiCard3(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    nomeRegione,
                    null
                    , s
                )
            )
            dataList.add(
                datiCard3(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    nomeRegione,
                    null
                    , s
                )
            )
            dataList.add(
                datiCard3(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    nomeRegione,
                    null
                    , s
                )
            )



            runOnUiThread {
                val rvAdapter = RvAdapter3(dataList)
                recyclerView.adapter = rvAdapter
                rvAdapter.onItemClick = { pos, view ->
                    if(pos == 6){
                        val b = Bundle()
                        val intent = Intent(this, regioni::class.java)
                        startActivity(intent)
                    }

                }
            }


        }
    }
}

    @RequiresApi(Build.VERSION_CODES.N)
    fun getConfermati(ita : Collection<DatiRegionali>, nomeRegio : String) : ArrayList<String> {
        val oggi = ita.last().data.substring(0,10)
        val ieri = get_yesterdayDate(oggi)
        val arr = ArrayList<String>()
        ita.forEach {
            if(it.denominazione_regione == nomeRegio && it.data.contains(oggi)){
                val ie = get_ieri(it.totale_casi,ieri,ita,nomeRegio,0)
                arr.add(it.totale_casi.toString())
                arr.add(ie.toString())
               return arr
            }
        }
    return ArrayList<String>()
    }
    fun get_ieri(dato : Int, ieri : String, it:Collection<DatiRegionali>, nom : String, scelta : Int) : Int{
        it.forEach {
            when(scelta){
            0->{
                if(it.denominazione_regione == nom && it.data.contains(ieri)){
                    return dato.minus(it.totale_casi)
                }
            }
                1->{
                    if(it.denominazione_regione == nom && it.data.contains(ieri)){
                        return dato.minus(it.dimessi_guariti)
                    }
                }
                2->{
                    if(it.denominazione_regione == nom && it.data.contains(ieri)){
                        return dato.minus(it.deceduti)
                    }
                }
            }

        }
        return 0
    }


    @RequiresApi(Build.VERSION_CODES.N)
    fun get_yesterdayDate(data : String) : String{
        val dateFormat: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val cal = Calendar.getInstance()
        val date: Date = dateFormat.parse(data)
        cal.time = date
        cal.add(Calendar.DATE, -1)
       return dateFormat.format(cal.time).toString()
    }
    @RequiresApi(Build.VERSION_CODES.N)
    fun getDeceduti(ita : Collection<DatiRegionali>, nomeRegio : String) : ArrayList<String> {
        val oggi = ita.last().data.substring(0,10)
        val ieri = get_yesterdayDate(oggi)
        val arr = ArrayList<String>()
        ita.forEach {
            if(it.denominazione_regione == nomeRegio && it.data.contains(oggi)){
                val ie = get_ieri(it.deceduti,ieri,ita,nomeRegio,2)
                arr.add(it.deceduti.toString())
                arr.add(ie.toString())
                return arr
            }
        }
        return ArrayList<String>()
    }
    @RequiresApi(Build.VERSION_CODES.N)
    fun getGuariti(ita : Collection<DatiRegionali>, nomeRegio : String) : ArrayList<String> {
        val oggi = ita.last().data.substring(0,10)
        val ieri = get_yesterdayDate(oggi)
        val arr = ArrayList<String>()
        ita.forEach {
            if(it.denominazione_regione == nomeRegio && it.data.contains(oggi)){
                val ie = get_ieri(it.dimessi_guariti,ieri,ita,nomeRegio,1)
                arr.add(it.dimessi_guariti.toString())
                arr.add(ie.toString())
                return arr
            }
        }
        return ArrayList<String>()
    }
}
