package com.example.orior.DataCard

import com.example.orior.DatiJson.Csv.DatiArray
import com.example.orior.DatiJson.DatiNazionali
import com.example.orior.DatiJson.DatiRegionali

class datiCard {
    var uno : String?
    var due : String?
    var tre : String?
    var naz : Collection<DatiNazionali>?
    var reg : Collection<DatiRegionali>?
    var Eu : DatiArray?
    constructor(uno : String?, due : String?, tre : String?, quattro : Collection<DatiNazionali>?, cinque : Collection<DatiRegionali>?,arr : DatiArray?){
        this.uno = uno
        this.due = due
        this.tre = tre
        this.naz = quattro
        this.reg = cinque
        this.Eu = arr
    }

}