package com.example.orior.DataCard

import com.example.orior.DatiJson.Csv.DatiArray
import kotlin.properties.Delegates

class PassValue {
    lateinit var Dati : DatiArray
    lateinit var Date : ArrayList<String>
    var flag : Boolean = false
    fun setdatiarr(dat : DatiArray){
        Dati = dat
    }
    fun setFlag1(dat : Boolean){
        flag = dat
    }
    fun getFlag1() : Boolean{
        return flag
    }

    fun getDati1() : DatiArray{
        return Dati
    }
    fun setDate1(date : ArrayList<String>){
        this.Date = date
    }
    fun getDate1() : ArrayList<String>{
        return Date
    }

    companion object
    {
        var shared = PassValue()
    }
}