package com.example.orior.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.DataCard.datiCard2
import com.example.orior.R
import kotlin.collections.ArrayList


class RvAdapter2(val userList: ArrayList<datiCard2>) : RecyclerView.Adapter<RvAdapter2.ViewHolder>() {

    var onItemClick: ((pos: Int, view: View) -> Unit)? = null
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {


        val v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_view2, p0, false)

        return ViewHolder(v);
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.regione.text = userList[p1].regione
        p0.confermati.text = p0.contesto.getString(R.string.conf)+"       " + userList[p1].confermati
        p0.deceduti.text = p0.contesto.getString(R.string.dec)+"       " + userList[p1].deceduti
        p0.guariti.text = p0.contesto.getString(R.string.guar)+"       " + userList[p1].guariti
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        override fun onClick(v: View) {
            onItemClick?.invoke(adapterPosition, v)
        }

        val regione = itemView.findViewById<TextView>(R.id.regione)
        val confermati = itemView.findViewById<TextView>(R.id.Confermati)
        val guariti = itemView.findViewById<TextView>(R.id.guariti)
        val deceduti = itemView.findViewById<TextView>(R.id.deceduti)
        val contesto = itemView.context

        init {
            itemView.setOnClickListener(this)
        }
    }
}